"""
Populates asset table with simulated stock records
Takes in a single argument - number of fake records to generated
Example usage:
python populate_db.py 25
"""

import sys
import os
import pymongo

from random import seed
from random import randint
from random import randrange
from random import uniform

from datetime import timedelta
from datetime import datetime


def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """

    delta = end - start
    int_delta = delta.days * 24 * 60 * 60 + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)


client = pymongo.MongoClient("mongodb://" + os.getenv('MONGO_HOST', 'localhost') + ":27017/")
db = client['test']
col = db['asset']

tickers = [
    'AAPL',
    'NVDA',
    'AMZN',
    'GOOG',
    'NIO',
    'TSLA',
    'MSFT',
]

num_tickers = len(tickers)

seed(420)

records = []

start_date = datetime.strptime('7/19/2021 9:30 AM', '%m/%d/%Y %I:%M %p')
end_date = datetime.strptime('7/23/2021 4:00 PM', '%m/%d/%Y %I:%M %p')

for i in range(int(sys.argv[1])):
    record = {
        'symbol': tickers[randint(0, num_tickers) - 1],
        'last': round(uniform(10.00, 800.00), 2),
        'volume': randint(10, 10000),
        'currency': 'USD',
        'exchange': 'NASDAQ',
        'bid': round(uniform(10.00, 800.00), 2),
        'ask': round(uniform(10.00, 800.00), 2),
        'open': round(uniform(10.00, 800.00), 2),
        'close': round(uniform(10.00, 800.00), 2),
        'low': round(uniform(10.00, 800.00), 2),
        'high': round(uniform(10.00, 800.00), 2),
        'quantity': randint(1, 100),
        'date': random_date(start_date, end_date)
    }
    records.append(record)

x = col.insert_many(records)