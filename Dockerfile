FROM python:3-alpine
WORKDIR /app
ADD requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt
COPY . /app/
ENV MONGO_HOST mongo
ENTRYPOINT ["python", "/app/populate_db.py"]